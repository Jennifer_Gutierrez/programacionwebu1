from django.contrib import admin

# Register your models here.

from .models import Jugadores
from .models import JugadoresCincinnari
from .models import JugadoresCleveland
from .models import JugadoresPitt
from .models import JugadoresBuffalo
from .models import JugadoresMiami
from .models import JugadoresPatriots
from .models import JugadoresJets
from .models import JugadoresHouston
from .models import JugadoresJacksonville
from .models import JugadoresIndianapolis
from .models import JugadoresTennessee
from .models import JugadoresDenver
from .models import JugadoresKansas
from .models import JugadoresChargers
from .models import JugadoresOakland
from .models import JugadoresChicago
from .models import JugadoresDetroit
from .models import JugadoresGreen
from .models import JugadoresMinnesota

@admin.register(Jugadores)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]
@admin.register(JugadoresCincinnari)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresCleveland)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresPitt)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresBuffalo)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresMiami)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresPatriots)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresJets)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresHouston)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresJacksonville)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresIndianapolis)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresTennessee)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresDenver)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresKansas)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresChargers)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresOakland)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresChicago)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresDetroit)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresGreen)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]

@admin.register(JugadoresMinnesota)
class AdminTweet(admin.ModelAdmin):
    list_display = [
        "nombre"
    ]