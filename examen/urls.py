"""examen URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from home import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.inicio, name="inicio"),
    path('equipos/', views.equipos, name="equipos"),
    path('estadios/', views.estadios, name="estadios"),
    path('detail/<int:id>',views.detail,name="detail"),
    path('PaginaBaltimore/', views.PaginaBaltimore, name="PaginaBaltimore"),
    path('PaginaCincinnari/', views.PaginaCincinnari, name="PaginaCincinnari"),
    path('PaginaCleveland/', views.PaginaCleveland, name="PaginaCleveland"),
    path('PaginaPittsburgh/', views.PaginaPittsburgh, name="PaginaPittsburgh"),
    path('PaginaBuffalo/', views.PaginaBuffalo, name="PaginaBuffalo"),
    path('PaginaMiami/', views.PaginaMiami, name="PaginaMiami"),
    path('PaginaPatriots/', views.PaginaPatriots, name="PaginaPatriots"),
    path('PaginaJets/', views.PaginaJets, name="PaginaJets"),
    path('PaginaHouston/', views.PaginaHouston, name="PaginaHouston"),
    path('PaginaJacksonville/', views.PaginaJacksonville, name="PaginaJacksonville"),
    path('PaginaIndianapolis/', views.PaginaIndianapolis, name="PaginaIndianapolis"),
    path('PaginaTennessee/', views.PaginaTennessee, name="PaginaTennessee"),
    path('PaginaDenver/', views.PaginaDenver, name="PaginaDenver"),
    path('PaginaKansas/', views.PaginaKansas, name="PaginaKansas"),
    path('PaginaChargers/', views.PaginaChargers, name="PaginaChargers"),
    path('PaginaOakland/', views.PaginaOakland, name="PaginaOakland"),
    path('PaginaChicago/', views.PaginaChicago, name="PaginaChicago"),
    path('PaginaDetroit/', views.PaginaDetroit, name="PaginaDetroit"),
    path('PaginaGreen/', views.PaginaGreen, name="PaginaGreen"),
    path('PaginaMinnesota/', views.PaginaMinnesota, name="PaginaMinnesota"),








    



]
