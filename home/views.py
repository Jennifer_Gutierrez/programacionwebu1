from django.shortcuts import render
from .models import Jugadores
from .models import JugadoresCincinnari
from .models import JugadoresCleveland
from .models import JugadoresPitt
from .models import JugadoresBuffalo
from .models import JugadoresMiami
from .models import JugadoresPatriots
from .models import JugadoresJets
from .models import JugadoresHouston
from .models import JugadoresJacksonville
from .models import JugadoresIndianapolis
from .models import JugadoresTennessee
from .models import JugadoresDenver
from .models import JugadoresKansas
from .models import JugadoresChargers
from .models import JugadoresOakland
from .models import JugadoresChicago
from .models import JugadoresDetroit
from .models import JugadoresGreen
from .models import JugadoresMinnesota

# Create your views here.

def inicio(request):
    context = {

    }
    return render(request, "home/inicio.html", context)

def PaginaBaltimore(request):
    queryset = Jugadores.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,"home/PaginaBaltimore.html", context)

def PaginaCincinnari(request):
    queryset = JugadoresCincinnari.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,"home/PaginaCincinnari.html", context)

def equipos(request):
    return render (request, "home/equipos.html", {})

def estadios(request):
    return render (request, "home/estadios.html", {})
def jugadores(request):
    return render (request, "home/jugadores.html", {})

def detail(request,id):
	queryset = JugadoresCincinnari.objects.get(id=id)
	context = {
		"object": queryset
	}
	return render(request,"home/detail.html",context)

def PaginaBuffalo(request):
    queryset = JugadoresBuffalo.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaBuffalo.html', context)

def PaginaMiami(request):
    queryset = JugadoresMiami.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaMiami.html', context)

def PaginaPatriots(request):
    queryset = JugadoresPatriots.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaPatriots.html', context)

def PaginaJets(request):
    queryset = JugadoresJets.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaJets.html', context)


def PaginaDenver(request):
    queryset = JugadoresDenver.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaDenver.html', context)

def PaginaKansas(request):
    queryset = JugadoresKansas.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaKansas.html', context)

def PaginaChargers(request):
    queryset = JugadoresChargers.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaChargers.html', context)

def PaginaOakland(request):
    queryset = JugadoresOakland.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaOakland.html', context)



def PaginaChicago(request):
    queryset = JugadoresChicago.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaChicago.html', context)

def PaginaDetroit(request):
    queryset = JugadoresDetroit.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaDetroit.html', context)

def PaginaGreen(request):
    queryset = JugadoresGreen.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaGreen.html', context)

def PaginaMinnesota(request):
    queryset = JugadoresMinnesota.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaMinnesota.html', context)


def PaginaCleveland(request):
    queryset = JugadoresCleveland.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,"home/PaginaCleveland.html", context)

def PaginaPittsburgh(request):
    queryset = JugadoresPitt.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaPittsburgh.html', context)

def PaginaHouston(request):
    queryset = JugadoresHouston.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaHouston.html', context)

def PaginaJacksonville(request):
    queryset = JugadoresJacksonville.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaJacksonville.html', context)

def PaginaIndianapolis(request):
    queryset = JugadoresIndianapolis.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaIndianapolis.html', context)

def PaginaTennessee(request):
    queryset = JugadoresTennessee.objects.all()
    context = {
        "list_jugadores": queryset
    }
    return render(request,'home/PaginaTennessee.html', context)