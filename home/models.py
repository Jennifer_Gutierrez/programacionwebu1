from django.db import models

# Create your models here.

class Jugadores(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresCincinnari(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresCleveland(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresPitt(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresBuffalo(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresMiami(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresPatriots(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresJets(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresHouston(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresJacksonville(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresIndianapolis(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresTennessee(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresDenver(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresKansas(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresChargers(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresOakland(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresChicago(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresDetroit(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresGreen(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre

class JugadoresMinnesota(models.Model):
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=120)
    edad = models.IntegerField()
    altura = models.FloatField()
    pesoKg = models.FloatField()

    
def __str__(self):
    return self.nombre